FROM debian:stretch-slim

RUN mkdir /activitywatch
WORKDIR /activitywatch
COPY . /activitywatch/
RUN apt-get -qq -y update \
  && apt-get install -qq -y --no-install-recommends ca-certificates unzip wget \
  && wget -q -O - https://github.com/ActivityWatch/activitywatch \
  | grep "https" \
  | grep "linux-x86_64" \
  | head -1 \
  | cut -d : -f 2,3 \
  | tr -d '", ' \
  | wget -q -i - \
  && chmod +x /activitywatch/ \
  && apt-get purge -qq -y --auto-remove ca-certificates unzip wget
# RUN useradd --home-dir /activitywatch --shell /bin/bash aw
# RUN chown -R aw:aw /activitywatch
# USER aw

EXPOSE 5600
SHELL ["/bin/bash", "-c"]
CMD ["/activitywatch/aw-server"]